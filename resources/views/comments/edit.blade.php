@extends('layout')

@section('content')

    <div class="card">
        <header class="card-header">
            <p class="card-header-title">
                コメントの編集
            </p>
        </header>

        <div class="card-content">
            <div class="content">

                <form method="POST" action="{{ route('comments.update', ['comment' => $comment]) }}">

                    @csrf
                    @method('PUT')

                    <div class="field">
                        <label class="label" for="user_name">投稿者名</label>
                        <div class="control">
                            <input id="user_name" name="user_name"
                                   class="input {{ $errors->has('user_name') ? 'is-danger' : '' }}" type="text"
                                   placeholder="スレッド投稿者名。" value="{{ old('user_name') ?: $comment->user_name }}">
                        </div>

                        @if($errors->has('user_name'))
                            <p class="help is-danger">{{ $errors->first('user_name') }}</p>
                        @endif

                    </div>

                    <div class="field">
                        <label class="label" for="message">メッセージ</label>
                        <div class="control">
                                <textarea id="message" name="message"
                                          class="textarea {{ $errors->has('message') ? 'is-danger' : '' }}"
                                          placeholder="Textarea">{{ old('message') ?: $comment->message }}</textarea>
                        </div>

                        @if($errors->has('message'))
                            <p class="help is-danger">{{ $errors->first('message') }}</p>
                        @endif
                    </div>

                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-link" type="submit">編集する</button>
                        </div>
                        <div class="control">
                            <a class="button is-text" href="{{ route('topics.show', ['topic' => $topic]) }}">キャンセル</a>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>

@endsection
