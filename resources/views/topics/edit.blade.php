@extends('layout')

@section('content')

    <div class="card">
        <header class="card-header">
            <p class="card-header-title">
                スレッドの編集
            </p>
        </header>

        <div class="card-content">
            <div class="content">

                <form method="POST" action="{{ route('topics.update', ['topic' => $topic]) }}">
                    @csrf
                    @method('PUT')


                    <div class="field">
                        <label class="label" for="title">スレッド名</label>
                        <div class="control">
                            <input id="title" name="title" class="input {{ $errors->has('title') ? 'is-danger' : ''}}"
                                   type="text" placeholder="スレッド名をここにいれてください。"
                                   value="{{ old('title') ?: $topic->title }}">
                        </div>
                        @if($errors->has('title'))
                            <p class="help is-danger">{{ $errors->first('title') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label class="label" id="user_name">投稿者名</label>
                        <div class="control">
                            <input id="user_name" name="user_name"
                                   class="input {{ $errors->has('user_name') ? 'is-danger' : '' }}" type="text"
                                   placeholder="スレッド投稿者名。"
                                   value="{{ old('user_name') ?: $topic->user_name }}">
                        </div>
                        @if($errors->has('user_name'))
                            <p class="help is-danger">{{ $errors->first('user_name') }}</p>
                        @endif
                    </div>


                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-link" type="submit">更新する</button>
                        </div>
                        <div class="control">
                            <a class="button is-text" href="{{ route('topics.show', ['topic'=>$topic]) }}">キャンセル</a>
                        </div>
                    </div>


                </form>

            </div>
        </div>
    </div>

@endsection
