@extends('layout')

@section('content')

    <div class="columns is-variable bd-klmn-columns is-2">
        <div class="column is-9">
            <h2 class="title is-5">スレッド一覧</h2>
        </div>
        <div class="column is-3">
            <div class="buttons are-small is-right">
                <a class="button is-link" href="{{ route('topics.create') }}">スレッドの作成</a>
            </div>
        </div>
    </div>

    @foreach($topics as $topic)
        <article class="card">
            <div class="card-content">
                <div class="content">
                    <h2 class="title is-6">{{ $topic->title }}</h2>
                    <div class="field is-grouped is-grouped-multiline">
                        <div class="control">
                            <span class="tag">投稿日時: <time datetime="{{ $topic->created_at->format('Y.m.d') }}">{{ $topic->created_at->format('Y.m.d') }}</span></time>
                        </div>
                        <div class="control">
                            <span class="tag">コメント {{ $topic->comments->count() }}件</span>
                        </div>
                    </div>
                    <p class="is-size-7"><span>@</span>{{ empty($topic->user_name) ? '名無しの人' : $topic->user_name }}</p>
                    <p class="buttons are-small"><a class="button is-link" href="{{ route('topics.show', ['topic'=>$topic]) }}">詳細を見る</a></p>
                </div>
            </div>
        </article>
    @endforeach

    {{-- TODO: paginationのテンプレートの指定方法がこれで良いか不明 --}}
    {{ $topics->links('vendor.pagination.default') }}

@endsection
