@extends('layout')

@section('content')

    <div class="card">
        <header class="card-header">
            <p class="card-header-title">
                {{ $topic->title }}
            </p>
        </header>
        <div class="card-content">
            <div>
                <div class="content">
                    <div class="field is-grouped is-grouped-multiline">
                        <div class="control">
                            <span class="tag">投稿日時: <time
                                    datetime="{{ $topic->created_at->format('Y.m.d') }}">{{ $topic->created_at->format('Y.m.d') }}</span></time>
                        </div>
                        <div class="control">
                            <span class="tag">コメント {{ $topic->comments->count() }}件</span>
                        </div>
                    </div>
                    <p class="is-size-7"><span>@</span>{{ empty($topic->user_name) ? '名無しの人' : $topic->user_name }}</p>
                </div>

                <div class="buttons are-small is-right">
                    <a href="{{ route('top') }}" class="button">スレッド一覧へ</a>

                    <a href="{{ route('topics.edit', ['topic'=>$topic]) }}" class="button is-link">編集する</a>

                    <form method="POST" action="{{ route('topics.destroy', ['topic' => $topic]) }}" style="display: inline-block;">
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="button is-danger btn-dell">削除する</button>
                    </form>

                </div>

                <!-- コメント -->
                <h2 class="title is-5">コメント</h2>
                <div class="box">

                    <form method="POST" action="{{ route('comments.store') }}">
                        @csrf

                        <input type="hidden" name="topic_id" value="{{ $topic->id }}">

                        <div class="field">
                            <label class="label" for="user_name">投稿者名</label>
                            <div class="control">
                                <input id="user_name" name="user_name"
                                       class="input {{ $errors->has('user_name') ? 'is-danger' : '' }}" type="text"
                                       placeholder="スレッド投稿者名。" value="{{ old('user_name') }}">
                            </div>

                            @if($errors->has('user_name'))
                                <p class="help is-danger">{{ $errors->first('user_name') }}</p>
                            @endif

                        </div>


                        <div class="field">
                            <label class="label" for="message">メッセージ</label>
                            <div class="control">
                                <textarea id="message" name="message"
                                          class="textarea {{ $errors->has('message') ? 'is-danger' : '' }}"
                                          placeholder="Textarea">{{ old('message') }}</textarea>
                            </div>

                            @if($errors->has('message'))
                                <p class="help is-danger">{{ $errors->first('message') }}</p>
                            @endif
                        </div>


                        <div class="field is-grouped">
                            <div class="control">
                                <button class="button is-link" type="submit">コメントをする</button>
                            </div>
                        </div>

                    </form>

                </div>


                <h2 class="title is-5">コメント一覧 ({{ $topic->comments->count() }})</h2>

                @if($comments->count())
                    <div class="box">

                        @foreach($comments as $comment)

                            <article class="media">
                                <div class="media-content">
                                    <h3 class="title is-6">{{ empty($comment->user_name) ? '名無しの人' : $comment->user_name }}</h3>
                                    <div class="content">
                                        <p>{!! nl2br(e($comment->message)) !!}</p>
                                        <p class="has-text-right">
                                            <small>{{ $comment->created_at->format('Y.m.d H:i') }}</small>
                                        </p>
                                    </div>
                                    <div class="buttons are-small is-right">
                                        <a href="{{ route('comments.edit', ['comment' => $comment]) }}" class="button is-link">編集する</a>

                                        <form method="POST" action="{{ route('comments.destroy', ['comment' => $comment]) }}" style="display: inline-block;">
                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="button is-danger btn-dell">削除する</button>
                                        </form>

                                    </div>
                                </div>
                            </article>

                        @endforeach

                    </div>

                    {{ $comments->links('vendor.pagination.default') }}
                @else
                    <p>まだ、コメントはありません。</p>
                @endif

            </div>
        </div>
    </div>

    <script>
        window.addEventListener('load', ()=>{
            var $btnDell = document.getElementsByClassName('btn-dell');
            for (var i = 0; $btnDell.length > i; i++) {
                $btnDell[i].addEventListener('click', function(e) {
                    if(!confirm("本当に削除しますか？")){
                        e.preventDefault();
                        console.log('削除を中止しました。');
                    }
                })
            }
        });
    </script>


@endsection
