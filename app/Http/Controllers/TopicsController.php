<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Topic;

class TopicsController extends Controller
{
    /*
     * スレッド一覧
     */
    public function index()
    {
        $topics = Topic::with(['comments'])->orderBy('created_at', 'desc')->paginate(10)->onEachSide(1);
        return view('topics.index', ['topics' => $topics]);
    }

    /*
     * 編集ページ
     */
    public function create()
    {
        return view('topics.create');
    }

    /*
     * 登録
     */
    public function store(Request $request)
    {
        $params = $request->validate([
            'user_name' => 'max:32',
            'title' => 'required|max:64',
        ]);

        Topic::create($params);

        return redirect()->route('top');
    }

    /*
     * 詳細表示
     */
    public function show($topic_id)
    {
        $topic = Topic::findOrFail($topic_id);
        $comments = $topic->comments()->paginate(25);

        return view('topics.show', ['topic' => $topic, 'comments' => $comments]);
    }

    /*
     * 編集画面
     */
    public function edit($topic_id)
    {
        $topic = Topic::findOrFail($topic_id);

        return view('topics.edit', [
            'topic' => $topic,
        ]);
    }

    /*
     * 更新処理
     */
    public function update($topic_id, Request $request)
    {
        $params = $request->validate([
            'user_name' => 'max:32',
            'title' => 'required|max:64',
        ]);

        $topic = Topic::findOrFail($topic_id);
        $topic->fill($params)->save();

        return redirect()->route('topics.show', ['topic' => $topic]);
    }

    /*
     * 削除処理
     */
    public function destroy($topic_id)
    {
        $topic = Topic::findOrFail($topic_id);

        DB::transaction(function () use ($topic) {
            // TODO: スレッドは削除ではなくフラグ管理したいかも
            $topic->comments()->delete();
            $topic->delete();
        });

        return redirect()->route('top');
    }
}
