<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Topic;

class CommentsController extends Controller
{
    /*
     * 登録処理
     */
    public function store(Request $request)
    {
        $params = $request->validate([
            'topic_id' => 'required|exists:topics,id',
            'user_name' => 'max:32',
            'message' => 'required|max:2048',
        ]);

        $topic = Topic::findOrFail($params['topic_id']);
        $topic->comments()->create($params);

        return redirect()->route('topics.show', ['topic' => $topic]);
    }

    /*
     * 編集画面
     */
    public function edit($comment_id)
    {
        $comment = Comment::findOrFail($comment_id);

        $topic = Topic::findOrFail($comment->topic_id);

        return view('comments.edit', [
            'comment' => $comment,
            'topic' => $topic,
        ]);
    }

    /*
     * 更新処理
     */
    public function update($comment_id, Request $request)
    {
        $params = $request->validate([
            'user_name' => 'max:32',
            'message' => 'required|max:2048',
        ]);

        $comment = Comment::findOrFail($comment_id);
        $comment->fill($params)->save();

        $topic = Topic::findOrFail($comment->topic_id);

        return redirect()->route('topics.show', ['topic' => $topic]);
    }

    /*
     * 削除処理
     */
    public function destroy($comment_id)
    {
        $comment = Comment::findOrFail($comment_id);
        $topic = Topic::findOrFail($comment->topic_id);

        DB::transaction(function () use ($comment) {
            // TODO: フラグ管理したいかも
            $comment->delete();
        });

        return redirect()->route('topics.show', ['topic' => $topic]);
    }
}
