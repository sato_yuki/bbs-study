<?php

use Illuminate\Database\Seeder;
use App\Topic;
use App\Comment;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Topic::class, 64)
            ->create()
            ->each(function ($post){
                $comments = factory(Comment::class, 10)->make();
                $post->comments()->saveMany($comments);
            });
    }
}
