<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'user_name'=>'名無しの太郎',
        'message'=>'この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。',
    ];
});
