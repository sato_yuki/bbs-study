<?php

use Faker\Generator as Faker;

$factory->define(App\Topic::class, function (Faker $faker) {
    return [
        'user_name'=>'名無しのスレッド主',
        'title' => 'スレッドタイトルがここに入ります。',
    ];
});
